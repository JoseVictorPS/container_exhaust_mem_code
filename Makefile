output: main.o gen_stack.o
	gcc -std=c99 main.o gen_stack.o -o output

main.o: main.c
	gcc -std=c99 -c main.c

gen_stack.o: gen_stack.c gen_stack.h
	gcc -std=c99 -c gen_stack.c

clean:
	rm *.o output

#target: dependencies
#	action
