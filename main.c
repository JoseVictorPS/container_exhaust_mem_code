#include "gen_stack.h"
#include <time.h>

int main(void) {

    clock_t comeco = clock();

    STK * pilha_um = create_s();
    /*STK * pilha_dois = create_s();
    STK * pilha_tres = create_s();
    STK * pilha_quatro = create_s();
    STK * pilha_cinco = create_s();*/

    long count = 0;

    while(count < 14) {
        push(pilha_um);
        /*push(pilha_dois);
        push(pilha_tres);
        push(pilha_quatro);
        push(pilha_cinco);*/
        printf("LACOS: %ld\n", count);
        count++;
    }

    int a = 0;
    while(a < 999999999) {
        a++;
    }
    printf("a = %d\n", a);
    int b = 0;
    while(b < 999999999) {
        b++;
    }
    printf("b = %d\n", b);
    int c = 0;
    while(c < 999999999) {
        c++;
    }
    printf("c = %d\n", c);

    free_s(pilha_um);
    /*free_s(pilha_dois);
    free_s(pilha_tres);
    free_s(pilha_quatro);
    free_s(pilha_cinco);*/
    printf("SIZE %d\n", INT_MAX);

    clock_t fim = clock();
    double tempo_gasto = (double)(fim - comeco) / CLOCKS_PER_SEC;
    printf("\nO TEMPO GASTO: %lf SEGUNDOS\n\n", tempo_gasto);

    return 0;
}
