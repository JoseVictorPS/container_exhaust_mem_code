#ifndef GENSTACK_H
#define GENSTACK_H

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

typedef struct lista { // Estrutura de pilha genérica(lista dinamica)
    char node[100000000];// Cada nó guarda um ponteiro genérico
    struct lista * prox; // Ponteiro para o próximo da pilha
}List;

typedef struct stack { //Descritor da pilha
    List * ini; // Ponteiro para o início da pilha
}STK;

STK * create_s (void);
void push( STK * f);
void pop(STK * f);
int empty_s(STK * f);
void free_s(STK * f);

#endif //GENSTACK_H
